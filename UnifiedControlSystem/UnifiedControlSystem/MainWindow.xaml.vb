﻿Imports SnmpSharpNet

Class MainWindow

    Private Sub Window_ContentRendered(sender As Object, e As EventArgs)

        Console.WriteLine("Programm gestartet.")
        Dim host As String = "localhost"
        Dim community As String = "public"
        Dim requestOid() As String
        Dim result As Dictionary(Of Oid, AsnType)
        requestOid = New String() {"1.3.6.1.2.1.1.1.0", "1.3.6.1.2.1.1.2.0"}
        Dim snmp As SimpleSnmp = New SimpleSnmp(host, community)
        If Not snmp.Valid Then
            Console.WriteLine("Invalid hostname/community.")
            Exit Sub
        End If

        result = snmp.Get(SnmpVersion.Ver1, requestOid)
        If result IsNot Nothing Then
            Dim kvp As KeyValuePair(Of Oid, AsnType)
            For Each kvp In result
                Console.WriteLine("{0}: ({1}) {2}", kvp.Key.ToString(),
                                  SnmpConstants.GetTypeName(kvp.Value.Type),
                                  kvp.Value.ToString())
            Next
        Else
            Console.WriteLine("No results received.")
        End If


    End Sub

End Class
